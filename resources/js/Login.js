/** Deals with simple login validation */
!function LoginValidation() {

	this.Container = $("#userLogin");
	this.FormElement = this.Container.children("form");
	this.userNameInput = this.FormElement.children("input[name=userName]");
	this.userPasswordInput = this.FormElement.children("input[name=userPassword]");
	this.inputs = [this.userNameInput, this.userPasswordInput];

	this.isValid = function() {

		if(this.userNameInput.val() == "" || this.userPasswordInput.val() == "") {
			if (this.userNameInput.val() == "") {
				this.userNameInput.addClass("error");
			}
			if (this.userPasswordInput.val() == "") {
				this.userPasswordInput.addClass("error");
			}
			return false;
		} else {
			return true;
		}
	}

	/** Add event listeners to both inputs */
	for (var i = 0; i < this.inputs.length; i++) {
		$(this.inputs[i]).on("keypress", function() {
			$(this).removeClass("error");
		});
	}

	$(this.FormElement).on("submit", {Context:this}, function(event) {
		var Context = event.data.Context;
		if(!Context.isValid()) {
			event.preventDefault();
		}
	});
	
}();