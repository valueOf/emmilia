function Form() {

	var form = $(".edit-form");
	var id = form.attr("id");

	var idMapping = {
		form_mitarbeiter : "ma_",
		form_kunden : "ku_",
		form_artikel : "ar_",
		form_bestellung : "or_"
	}

	var closeBtn = form.children(".close-form");
	var overlay = $("#form-overlay");
	var inputs = form.find("input").not("input[name=form-submit]");
	var submitBtn = form.find("input[name=form-submit]");
	
	function resetFields() {
		$(".edit-form .inputfield").val("");
	}
	
	function getData(pConstraints) {
		
		var dataContainer = pConstraints.caller.closest('tr').children('td');
		dataContainer.each(function() {
			var fieldName = $(this).attr('data-name');
			var fieldValue = $(this).attr('data-value');
			$('.edit-form [name="' + fieldName + '"]').val(fieldValue);
		});
		
	}

	$(".edit").on("click", function() {
		resetFields();
		getData({
			caller: $(this),
			table: id.replace("form_", ""),
			id: $(this).siblings("td[data-name="+idMapping[id]+"id_number]").text()
		});

		form.show();
		overlay.show();
	});

	$(".delete").on("click", function() {
		$response = confirm("Wirklich löschen?");
		if( true == $response ) {
			var data = { 
					'Klasse' : $('#data_class').val()
					};
			
			var dataContainer = $(this).closest('tr').children('td');
			dataContainer.each(function() {
				var fieldName = $(this).attr('data-name');
				var fieldValue = $(this).attr('data-value');
				data[fieldName] = fieldValue;
			});
			var _that = this;
			$.post('/DataManipulation.responder.php?action=delete', data, function( response ) {
				console.log(response);
				if(true == response.success ){
					$(_that).closest('tr').remove();
				}
			});
		}
	});
		

	closeBtn.on("click", function(){
		form.hide();
		overlay.hide();
	});

	overlay.on("click", function(){
		form.hide();
		overlay.hide();
	});

	function validate() {
		var valid = true;
		var prefix = idMapping[id];

		inputs.each(function() {

			var currentName = $(this).attr("name");
			var currentType = $(this).attr("type");
			var currentVal = $(this).val();

			$(this).removeClass("error");

			if( typeof $(this).attr("required") != "undefined") {
				if (currentVal.length <= 0) {
					$(this).addClass("error");
					valid = false;
				}
			}

			if (currentType == "text") {
				var containsNumber = /\d/.test(currentVal);
				if (containsNumber) {
					$(this).addClass("error");
					valid = false;
				}
			} else if (currentType == "number") {
				if (currentName == prefix+"adress_postalcode") {
					if (currentVal.length > 5) {
						$(this).addClass("error");
						valid = false;
					}
				}
			}

		});
		return valid;
	}


	/*
	inputs.on("keypress", function() {
		validate();
	});
	*/
};

Form();

$(document).on("click", "input[name=form-submit]",  function(e) {
	/*if (!validate()) {
		e.preventDefault();
		console.log("Not Submitted");
	} else {*/
		e.preventDefault();
		var data = { 
				'Klasse' : $('#data_class').val()
				};
		$('.edit-form .inputfield').each(function() {
			data[$(this).attr('name')] = $(this).val();
		});
		var newData = false;
		$.post('/DataManipulation.responder.php?action=save', data, function( response ) {
			location.reload();
		});
	//}
});
/* Hubs on click ajax call */
$(".form-call").on("click", function(event) {
	event.preventDefault();

	var formTemplate = $(this).attr("href");
	
	$.ajax({
	  	url: formTemplate
	}).done(function(data) {

		$(".edit-form").remove();

		var form = $(data);
		var overlay = $("#form-overlay");

	  	$("body").append(form);

	  	form.show();
	  	overlay.show();

	  	Form();
	});

});


/* Testing */
!function testing() {
	var test = false;
	if (test) {
		console.log("--- testing ---");

		var editBtn = $(".edit").first();

		editBtn.click();

		var form = $(".edit-form");
		var submitBtn = form.find("input[name=form-submit]");

		submitBtn.click();
		console.log("--- testing ---");
	}
}();