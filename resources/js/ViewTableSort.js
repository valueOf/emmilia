/** Frontend sorting. May be deprecated by backend sorting */
!function ViewTableSort() {

	this.sortable = $(".view").find(".sort").parent();

	this.sortable.on("click", function() {

		var sortBy = $(this).data().name;

		/** Actual sorting process */
		function tableSort(pOrder) {
			var singleViews = $(".view-single").sort(function(a,b) {
					var c = $(a).find("td[data-name='"+sortBy+"']").text();
					var d = $(b).find("td[data-name='"+sortBy+"']").text();

					if (pOrder == "asc") {
						if(c < d) {
					        return -1;
					    } else if (c > d) {
					        return 1;
					    } else {
					    	return 0;
					    }
					} else if (pOrder == "desc") {
						if(c > d) {
					        return -1;
					    } else if (c < d) {
					        return 1;
					    } else {
					    	return 0;
					    }
					}
				});

			$(".view-single").remove();
			$(".view").append(singleViews);
			/* Reset Listeners */
			Form();
		}

		var sortButton = $(this).children(".sort");
		/** Display */
		if (sortButton.hasClass("desc") || (!sortButton.hasClass("desc") && !sortButton.hasClass("asc"))) {
			tableSort("asc");
			sortButton.addClass("asc");
			sortButton.removeClass("desc");
		} else {
			tableSort("desc");
			sortButton.addClass("desc");
			sortButton.removeClass("asc");
		}

		$(this).addClass("sorted");
		$(this).siblings().removeClass("sorted");

	});

}();