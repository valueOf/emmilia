function Warenkorb() {
	this.artikel = {};
	this.summe = 0;
	this.initEventHandlers();
	this.Versandkosten = 0;
};
Warenkorb.prototype.initEventHandlers = function() {
	var _that = this;
	$('section.kasse.produkte').on('click', '.produkt_item', function(e) {
		_that.addArtikel(e);
	});
	$('section.kasse.warenkorb').on('click', '.delete', function(e) {
		_that.removeArtikel(e);
	});
	$('section.kasse.warenkorb .buylieferung').on('click', function(e) {
		var artikel = Object.keys(_that.artikel).length;
		if(artikel == 0) return alert("Keine Artikel im Warenkorb");
		_that.showLieferungDialog();
	})
	$('.clientradio').on('change', function(e) {
		_that.switchLieferungsKundenDialog(e);
	})
	$(document).on('click', 'button.buy', function(){
		_that.payLieferung();
	})
	$('section.kasse.warenkorb .buydirekt').on('click', function(e) {
		var artikel = Object.keys(_that.artikel).length;
		if(artikel == 0) return alert("Keine Artikel im Warenkorb");
		_that.showDirektDialog();
	})
};
/**
 * Fügt einen Artikel dem Warenkorb hinzu
 * Sofern er schon vorhanden ist wird die Anzahl ( Quantity ) erhöht
 */
Warenkorb.prototype.addArtikel = function(e) {
	var dataContainer = $(e.target).closest('.produkt_item');
	if(dataContainer.hasClass('disabled')) return alert('Lagerbestand des Artikels leer');

	var ID = dataContainer.attr('data-id');

	//check availability
	var Produkt = {
			Name : dataContainer.attr('data-name'),
			Preis: dataContainer.attr('data-preis')/100,
			Produktanzahl : 1
	};
	
	
	
	if(typeof this.artikel[ID] == 'undefined') {
		// Wenn nicht existiert, neu erstellen
		var Produktanzahl = 1;
	}else{
		var Produktanzahl = this.artikel[ID].Produktanzahl*1 + 1;
	}
	var _that = this;
	this.checkProduktAnzahl(Produktanzahl, ID, function() {
		Produkt.Produktanzahl = Produktanzahl;
		_that.artikel[ID] = Produkt;
		
		_that.updateList();
		_that.calcSum();
		return true;
	}, function() {
		alert("Produkt Lagerbestand zu gering");
		return false; 
	});

};
/**
 * Entfernt einen Artikel aus dem Warenkorb
 */
Warenkorb.prototype.removeArtikel = function( e ) {
	var dataContainer = $(e.target).closest('tr');
	var ID = dataContainer.attr('data-id');

	var amount = this.artikel[ID].Produktanzahl;
	if( amount > 1 ) {
		this.artikel[ID].Produktanzahl --
	}else{
		delete this.artikel[ID];
	}
	this.updateList();
}
Warenkorb.prototype.checkProduktAnzahl = function( Produktanzahl, Artikelnummer, Success, Failure ) {
	var data = {
		Artikelnummer : Artikelnummer,
		Aritkelanzahl : Produktanzahl
	};
	$.post('/DataManipulation.responder.php?action=checkBestand', data, function( response ) {
		if( true == response ) {
			Success();
		}else{
			Failure();
		}
	});	
}
/**
 * Aktualisiert die HTML Tabelle
 */
Warenkorb.prototype.updateList = function() {
	$('#warenkorbtable tbody tr').remove();
	for(var key in this.artikel) {
		var artikel = this.artikel[key];
		var element = $('<tr data-id="' + key + '"><td><a class="delete" href="#">X</a></td><td>' + artikel.Produktanzahl + 'x </td><td>' + artikel.Name + '</td><td>' + artikel.Preis + ' €</td></tr>');
		//$('#warenkorbtable tbody').append(element);
		element.appendTo('#warenkorbtable');
	}
	this.calcSum();
}
/**
 * Berechnet die Summe aller Artikel
 */
Warenkorb.prototype.calcSum = function() {
	var summe = 0;
	for(var key in this.artikel) {
		summe += this.artikel[key].Produktanzahl * this.artikel[key].Preis;
	}
	if( parseInt(this.summe) >= Versandkostengrenze/100 ) {
		$('.versand-tr').css('text-decoration', 'line-through');
	}else{
		$('.versand-tr').css('text-decoration', '');
		summe += parseFloat(this.Versandkosten)*1;
	}
	this.summe = (Math.round(summe * 100) / 100).toFixed(2);
	
	$('#warenkorb_summe').html(this.summe + ' €');
	return this.summe;
}
Warenkorb.prototype.showLieferungDialog = function() {
	var dialog = $("#form-overlay, #lieferungdialog");
	this.showVersand();
	dialog.show();
}
Warenkorb.prototype.switchLieferungsKundenDialog = function(e) {
	var caller = $(e.target);
	if(caller.val() == 'new') {
		$('.inputfield').val('');
	}
	$( '.clientoption' ).hide();
	$( '.clientoption.' + caller.val() ).show(); 
}
/**
 * Läutet die Beendigung eines Bezahlvorgangs ein
 */
Warenkorb.prototype.payLieferung = function() {
	var Kunde = this.getKundenDaten();
	if(this.summe >= Versandkostengrenze) Versandkosten = 0;
	
	var data = {
		Lieferung 		: "extern",
		Seller 			: Seller,
		Kunde			: Kunde,
		Artikel			: this.artikel,
		Versandkosten	: Versandkosten,
		Summe			: this.summe
	};
	$.post('/DataManipulation.responder.php?action=lieferung', data, function( response ) {
		if(response.success == true) {
			alert('Erfolgrei Bestellt');
			location.reload();
			
		}else{
			alert(response.msg);
			return false;
		}
	});
}
Warenkorb.prototype.showVersand = function() {
	console.log(Versandkosten);
	this.Versandkosten = (Versandkosten / 100).toFixed(2);
	
	$('#versandkosten').html(this.Versandkosten + " €");
	$('.versand-tr').fadeIn(200);
	this.calcSum();
}
Warenkorb.prototype.hideVersand = function() {
	this.Versandkosten = 0;
	$('.versand-tr').fadeOut(200);
	this.calcSum();
}
Warenkorb.prototype.getKundenDaten = function() {
	var kunde = {};
	$('#form_kunden .inputfield').each(function() {
		kunde[$(this).attr('name')] = $(this).val();
	})
	return kunde;
}
Warenkorb.prototype.showDirektDialog = function() {
	this.hideVersand();
}
$(document).ready(function() {
	new Warenkorb();
});