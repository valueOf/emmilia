$(document).on('click', '.color-rippling-container, .rippling',  function(e){
    var x = e.pageX;
    var y = e.pageY;
    var clickY = y - $(this).offset().top;
    var clickX = x - $(this).offset().left;
    if($(this).hasClass('rippling')) {
		var box = $(this).find('.color-rippling-container');
    }else{
	  	var box = this;
    }
   
  	var setX = parseInt(clickX);
	var setY = parseInt(clickY);
  	var svg = $('<svg><circle cx="'+setX+'" cy="'+setY+'" r="'+0+'"><animate  fill="freeze"  attributeName="r" from="0" to="'+$(box).outerWidth()+'" dur="0.5s"/><animate attributeName="opacity" from="1" to="0" fill="freeze" dur="0.5s"/></circle></svg>').appendTo($(box));
		setTimeout(function() {
		$(svg).remove();
	},1000);
});