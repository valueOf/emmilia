<?php
session_start();
// Configs
setlocale(LC_ALL, "de_DE.UTF-8");

define("APP_BASE_PATH", dirname(__DIR__));
define("APP_BASE_URL", "http://singinggcode/");
define("PDO_DEBUG", true); //PDO Debugging dumpt queries
// Passwort Hashing
// Bei veränderung muss jedes Passwort neu erstellt werden!!
define("PEPPER", "#Qi§Yjo$/*9=fR-70;lDvX)öÜYtQMdSG?dGIx=28MCqhgoH6**!Bqj/ß_W=tk38M"); //SECUUURE
define("HASH_ITERATIONS", 1500);
define("HASH_ALGO", "SHA512");
define("SALT_LENGTH", 32);


// Database
define("DB_USERNAME", "Emmy");
define("DB_PASS", "38zpsx3mPJEzzPst");
define("DATABASE", "emmilia");

//Versand in cent
define("VERSANDKOSTEN", 499);
define("VERSANDKOSTENGRENZE", 5000);

// Autoload
spl_autoload_register( "emmilia_autoload" );

function emmilia_autoload( $classname ) {
	//Wenn Klasse exisitert Lade Klasse
	if ( file_exists( APP_BASE_PATH . "/lib/". $classname .".class.php" ) ) {
		require_once( APP_BASE_PATH . "/lib/". $classname .".class.php" );
	}
	//Andere Autoloader
	else{
		return false;
	}
}