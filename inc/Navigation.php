<?php

$currentPage = basename($_SERVER['REQUEST_URI']);

$pages = array(
    APP_BASE_URL . 'Verwaltung/' => 'Home',
    APP_BASE_URL . 'Verwaltung/Mitarbeiter/' => 'Mitarbeiter',
    APP_BASE_URL . 'Verwaltung/Kunden/' => 'Kunden',
    APP_BASE_URL . 'Verwaltung/Artikel/' => 'Artikel',
    APP_BASE_URL . 'Verwaltung/Artikelgruppen/' => 'Artikel<wbr>kategorien',
    APP_BASE_URL . 'Verwaltung/Kasse/' => 'Kasse',
);
?>
<nav id="menu" class="low">
	<div class="toggle-nav"> 
	</div>
    <ul id="menuList" >
        <?php 
	        foreach ($pages as $path => $pageTitle) { 
	            if (basename($path) == $currentPage) { 
	        		echo "<li class='active $pageTitle'> $pageTitle </li>";
	        	} else {
	        		echo "<li class='$pageTitle'><a href='$path'> $pageTitle </a></li>";
	            }
	        }
        ?>
        <li><a id="logout" href='<?= APP_BASE_URL?>'> Log Out </a></li>
    </ul>
</nav>