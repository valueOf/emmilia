<?php
	$loggedIn = false;
	if(array_key_exists("USER_ID", $_SESSION)) {
		$loggedIn = true;
	}
    if (!$loggedIn && $title != "Login") {
        header('Location: ' . APP_BASE_URL);
        exit;
    }
?>

<!DOCTYPE html>
<html lang="de">
<head>
    <meta charset="UTF-8">
    <title>Emmilia - <?php echo $title ?></title>
    <script src="/resources/js/jquery-3.1.1.min.js"></script>
    <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Raleway">

    <script type="text/javascript" src="/resources/js/Helper.js"></script>
    <link type="text/css" rel="stylesheet" href="/resources/css/Main.css">
    <link type="text/css" rel="stylesheet" href="/resources/css/UserLogin.css">
    <?php if ($title != "Login") { ?> 
        <link type="text/css" rel="stylesheet" href="/resources/css/ViewTables.css">
        <link type="text/css" rel="stylesheet" href="/resources/css/Forms.css">
        <link rel="shortcut icon" href="/resources/img/kolibri4.png"/>
    <?php } ?>
</head>
<body>

<?php
    if( $loggedIn ) {
        require_once(APP_BASE_PATH . "/inc/Navigation.php" );
    }
?>

<main class="<?php if ($title != "Login") { echo 'width-90'; }?> ">

    <header class='main-header'>
        <h1>
            <img height="24px" src="/resources/img/kolibri4.png">
            <a href="index.html">Emmilia</a>
            <img height="24px" src="/resources/img/kolibri4.png">
        </h1>
    </header>

<?php ?>