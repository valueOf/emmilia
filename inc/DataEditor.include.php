<?php
    require_once($_SERVER["DOCUMENT_ROOT"] . "/inc/Header.include.php");
    if(!array_key_exists("fieldFilter", $_ENV)) $_ENV["fieldFilter"] = array();
?>
	<div class="maincontent">
	    <h2><?= $title?></h2>
		<form action="" method="GET">
			<label>
				Suchen nach: 
				<input value="<?= isset($_GET["q"]) ? $_GET["q"] : "";?>" class="low" name="q" type="text" >
				<?php if(isset($_GET["q"])) :?>
				<?php endif;?>
				<button class="btn low" type="submit" >Nicht suchen, finden!</button>
			</label>
		</form>
		<hr>
		<button class="button btn high edit" >Neuen <?= $Klasse?> anlegen<div class="color-rippling-container"></div></button>
		<?php 
			$tables = new $Klasse();
			
			$search = false;
			if( isset($_GET["q"]) ) {
				$search = $_GET["q"];
			}
			
			$tableFields = $fields;
			$AllObjects = $Klasse::search( $search );
			
			echo '<input type="hidden" id="data_class" value="' . $Klasse. '">';
			echo '<table id="'.$Klasse.'" class="view low">';
			echo '<tr class="view-header">';
			/* Table Head */
			foreach( $tableFields as $field=>$properties ) {
				if(in_array($field, $hiddenFields)) continue;
				echo '<th data-name="' . $field . '" >' . $field . '<div class="sort"></div><div class="color-rippling-container"></div></th>';
			}
			echo '<th>Editieren<div class="color-rippling-container"></div></th>';
			echo '<th>Löschen<div class="color-rippling-container"></div></th>';
				
			
			echo '</tr>';
			
			/* Table Content */
			foreach( $AllObjects as $Object ) {
				echo '<tr class="view-single">';
				foreach( $tableFields as $field=>$value ) {
					if(in_array($field, $hiddenFields)) continue;
					$class = "";
					echo '<td class="' .  $class .'" data-value="'.htmlspecialchars($Object->$field).'" data-name="'.$field.'">';
			
					if(array_key_exists($field, $_ENV["fieldFilter"])) {
						echo $_ENV["fieldFilter"][$field]($Object->$field);
					}else {
						echo $Object->$field;
					}
					
					echo "</td>";
					
				}
				echo '<td><a class="edit" href="#"></a></td>';
				echo '<td><a class="delete" href="#"></a></td>';
				echo "</tr>";
			}
			echo '</table>';

			include_once( $formPath );
			//$tables->createFormular();
		?>
		
	</div>
    <div class="color-rippling-container"></div>
    
<?php
	require_once($_SERVER["DOCUMENT_ROOT"] . "/inc/Footer.include.php");    
?>
    <!-- 
    <script>
    
    	$(document).ready(function() {
			$('.openmodal').on('click', function() {

				resetFormatValues();
				
				if($(this).hasClass('edit') ) {
					var dataContainer = $(this).closest('tr').children('td');
					dataContainer.each(function() {
						var fieldName = $(this).attr('data-field-name');
						var fieldValue = $(this).html();
						$('#' + fieldName).val(fieldValue);
					});
				}
				$('#modal').show();
			});

			$(document).on('click', '#modal .cancel', function() {
				resetFormatValues()
				$('#modal').hide();
			});

			$(document).on('click', '#modal .save', function() {

				var data = { 'Klasse' : $('#data_class').val() };

				$('#modal .inputfield').each(function() {
					data[$(this).attr('name')] = $(this).val();
				});
					
				$.post('/DataManipulation.responder.php?action=save', data, function( response ) {
					console.log(response);
				});
			});
			$(document).on('click', '.delete', function() {

				$response = confirm("Wirklich löschen?");
				if( true == $response ) {
					var data = { 
							'Klasse' : $('#data_class').val()
							};
					
					var dataContainer = $(this).closest('tr').children('td');
					dataContainer.each(function() {
						var fieldName = $(this).attr('data-field-name');
						var fieldValue = $(this).html();
						data[fieldName] = fieldValue;
					});
						
					$.post('/DataManipulation.responder.php?action=delete', data, function( response ) {
						console.log(response);
					});
				}
			});
    	});		

    	function resetFormatValues() {
			$('#modal table input, #modal table select').val('');
    	}
    </script> -->