<?php
abstract class DBController{
	/**
	 * Variable gibt den Table Namen an
	 */
	public static $table;

	/**
	 * Gibt alle Felder inklusive eigenschaften der Tabelle an
	 */
	protected static $fields;

    /**
     * Database
     */
    protected static $database = "emmilia";

    /**
     * Das Select sql, kann auch JOINS beinhalten
     * @var string
     */
	protected static $selectSQL; 
	
	/**
	 * Sql for creating the Table
	 * @var string
	 */
	protected static $migrationSQL;
	
	public static $primaryKey;
	
	public function __construct( $data = null ) {
	    if( $data ) {
            $this->apply( $data );
        }
	}
	/* -------------------------------------------------- BASE -------------------------------------------------- */
	

	/**
	 * qwertzy
	 *
	 */
	private static function fetch( $stmt ) {
		$return = array();
		$class = static::class;
		while( $row = $stmt->fetch() ) {
			$return[] = new $class($row);
		}
		return $return;
	}

	/**
	 * Holt sich einen einzelnen Datensatz mit dem Primarykey
	 * @param mixed $Value
	 * @param string $field
	 */
	public static function get( $primaryKey, $field=null ) {
        $sql = "SELECT * FROM " . static::$database . "." . static::$table . " WHERE " . static::$primaryKey . "=:primaryKey";
        $DB_connection = self::getPdoConnection();

        $stmt = $DB_connection->prepare( $sql );
        self::executeStatement($stmt, array(":primaryKey" => $primaryKey));

        $arr = self::fetch( $stmt );

        return array_pop( $arr );
	}
	/**
	 * Holt sich einen einzelnen Datensatz mit dem Primarykey
	 * @param mixed $Value
	 * @param string $field
	 */
	public static function getBy( $where , $bindFields=null, $order=false ) {
		if( !$where ) return false;

		$sql = "SELECT * FROM " . static::$database . "." . static::$table . " WHERE $where $order";
		$DB_connection = self::getPdoConnection();
		$stmt = $DB_connection->prepare( $sql );
		self::executeStatement($stmt, $bindFields);
	
		return self::fetch( $stmt );
	}	
	/**
	 * Holt sich alle Datensätze
	 */
	public static function getAll() {
		$sql = "SELECT * FROM ".static::$database.".".static::$table;
		
		$DB_connection = self::getPdoConnection();
		$stmt = $DB_connection->prepare( $sql );
		
		self::executeStatement( $stmt );
		return self::fetch( $stmt );
	}
	
	/**
	 * Volltextsuche, holt sich INDEX automatisch aus der Datenbank ( Index muss Fulltext heißen )
	 */
	public static function search( $search = false ) {
		$index = self::getIndex();
		
		if( !isset($index["Fulltext"]) || !$search ) {
			return self::getAll();
		}
		
		if(!$search) return self::getAll();
		
		$matchVariables = " (`".implode("`, `", $index["Fulltext"])."`)";
		
		$DB_connection = self::getPdoConnection();
		
		$quoted_search = $DB_connection->quote( "*$search*" );
		$sql = "SELECT * FROM ".static::$database.".".static::$table . " WHERE MATCH $matchVariables AGAINST($quoted_search in boolean mode) OR " . static::$primaryKey ."=:search";
		$stmt = $DB_connection->prepare( $sql );
		$stmt->bindParam(":search", $search);
		self::executeStatement( $stmt );
		return self::fetch( $stmt );
	}

	/**
	 * 
	 */
	public static function join( $fields, $join, $table, $using, $where, $order ) {
		$sql = "SELECT $fields FROM ".static::$database.".".static::$table . " $join $table $using $where $order";
		$DB_Connection = self::getPdoConnection();
		$stmt = $DB_Connection->prepare( $sql );
		self::executeStatement( $stmt );
		return self::fetch( $stmt );
	}
	/**
	 * Speichert den aktuellen Datensatz
	 */
	public function save() {

		$primaryKey = static::$primaryKey;
		
		if( isset($this->$primaryKey) ) {
			return $this->update();
		}else{
			return $this->insert();
		}
	}
	/**
	 * Updated den Datensatz
	 */
	private function update() {
		
		self::getFields();
		
		$updateFields = array();
		$bindFields = array();
		
		$values = "";
		
		$primaryKey = static::$primaryKey;
		
		foreach( $this as $field => $value ){
			//PK natürlich nicht updaten
			if( $field == $primaryKey ) continue;
			if( isset($value) ) {
				if($value == "%NULL%") $this->$field = null;
				if( array_key_exists( $field, static::$fields ) )
				{
					if(!empty($values)) $values.= ", ";
					$values .= "$field=:$field";
					$bindFields[":$field"] = $this->$field;
				}
			}
		}
		$bindFields[":primaryKey"] = $this->$primaryKey;
		$sql = "UPDATE " . static::$database . "." . static::$table ." SET $values WHERE $primaryKey=:primaryKey";
		$DB_connection = self::getPdoConnection();
		$stmt = $DB_connection->prepare( $sql );
		return self::executeStatement( $stmt, $bindFields );
	}
	/**
	 * Inserted den Datensatz
	 */
	private function insert() {

		self::getFields();

		$updateFields = array();
		$bindFields = array();

		foreach( $this as $field => $value ){
			if( $value ) {
				if( array_key_exists( $field, static::$fields ) )
				{
	                $updateFields[]   = $field;
	                $bindFields[":$field"]  = $value;
				}
			}
		}
		if(!$updateFields) return false;
		
        // Alle Values werden in ein SQL Statement gepackt Values werden mit : prefix für PDO binding versehen
        $fields = " (`".implode("`, `", $updateFields)."`)";
        $values = " VALUES (:".implode(", :", $updateFields).") ";
		$primaryKey = static::$primaryKey;
	    $sql = "INSERT INTO " . static::$database . "." . static::$table ." $fields $values" ;
	    
        $DB_connection = self::getPdoConnection();
        $stmt = $DB_connection->prepare( $sql );
        $success = self::executeStatement( $stmt, $bindFields );
        if( !$this->$primaryKey ) {
        	$this->$primaryKey = $DB_connection->lastInsertId();
        }

        return $success;
	}
	public function delete() {
		$DB_Connection = self::getPdoConnection();
		$primaryKey = static::$primaryKey;
		$stmt = $DB_Connection->prepare( "DELETE FROM " . static::$database . "." . static::$table ." WHERE ". $primaryKey."=:PrimaryKey");
		$stmt->bindParam(":PrimaryKey", $this->$primaryKey);
		$success = self::executeStatement( $stmt );
		static::getFields();
		foreach( static::$fields as $field=>$properties ) {
			unset($this->$field);
		}
		return $success;
	}
	/**
	 * Wenn bereits eine Datenbankverbindung existiert dann jot, wenn nich dann mach eine
	 */
	public static function getPdoConnection() {
		if( isset( $_ENV['DB_CONNECTION'] ) ) {
			return $_ENV['DB_CONNECTION'];
		}else{
			try {
				return new PDO("mysql:host=127.0.0.1;dbname=" . DATABASE  , DB_USERNAME, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
			} catch( PDOException $e ) {
				trigger_error( $e->getMessage() );
				return false;
			}
		}
	}
	/**
	 * Holt sich Tabellenstruktur
	 * ( Spaltennamen und typen )
	 */
	public static function getFields() {
		if( !static::$fields ) {
			$DB_connection = self::getPdoConnection();
			$sql = "SELECT
					    column_name,
					    column_type     
					FROM information_schema.columns 
					WHERE table_name= :table ";
			$stmt = $DB_connection->prepare( $sql );
			$stmt->bindParam(':table', static::$table);

			self::executeStatement($stmt);
			
			$allResults = array();
			while ( $row = $stmt->fetch() ) {
				$allResults[$row["column_name"]] = $row;
			}
			static::$fields = $allResults;
		}
		return static::$fields;
	}

	/**
	 * Get Indizes für FULLTEXT Suche
	 */
	private static function getIndex() {
		$sql = "SHOW INDEX FROM ".static::$table ." FROM ".static::$database;
		$DB_Connection = self::getPdoConnection();
		$stmt = $DB_Connection->prepare( $sql );
		self::executeStatement( $stmt );
		
		$allResults = array();
		while ( $row = $stmt->fetch() ) {
			$allResults[$row["Key_name"]][] = $row["Column_name"];
		}
		return $allResults;		
	}
	
	/**
	 * Führt das Sql statement aus. Bei Error überprüft es korrekte migration des Tables
	 */
	private static function executeStatement( $stmt, $params=null ) {
			$success = $stmt->execute($params);
			if( !$success ) {
				self::checkTableMigration();
				trigger_error( print_r( $stmt->errorInfo(), 1) );	
				return $success;
			}
			return $success;
	}
	
    /**
     * Speist einen Datensatz in das Objekt ein
     * @param $data array
     */
	public function apply( $data ) {
	    foreach( $data as $key=>$value ) {
	        if($value) $this->$key = $value;
        }
        return $this;
    }
    
    /* -------------------------------------------------- MIGRATION -------------------------------------------------- */
    /**
     * Überprüft ob die Tabelle existiert
     */
	private static function checkTableMigration() {
		$sql = "SHOW TABLES LIKE :TABLE";
		$DB_Connection = self::getPdoConnection();
		$stmt = $DB_Connection->prepare( $sql );
		$stmt->bindParam( ":TABLE", static::$table );
		$stmt->execute();
		// Wenn nicht gefetcht werden kann dann existiert Tabelle nicht
		if( !$stmt->fetch() ) {
			//Wenn nicht existiert dann existiere nun!
			self::migrateTable();
		}
	}
	
	/**
	 * Migriert die Tabelle
	 */
	private static function migrateTable() {
		$DB_Connection = self::getPdoConnection();
		$stmt = $DB_Connection->prepare( static::$migrationSQL );
		self::executeStatement( $stmt );
	}
	
	/* -------------------------------------------------- UTILITY -------------------------------------------------- */
	
	public static function getFormularFields() {
		return self::getFields();
	}
	
	public function createFormular() {
		$fields = static::getFormularFields();
		
		
		echo '<div class="edit_form" id="modal" class="modal high formular"><div class="color-rippling-container"></div>';
		echo '<h2>' . get_called_class() .' anlegen/editieren </h2>';
		echo '<form >';
		echo '<table>';
		foreach( $fields as $field=>$properties ) {
			if(in_array($field, static::$hiddenProperties)) continue;
				echo '<tr>';
					echo '<td>';
						echo '<label for="' . $field . '">'. $field .'</label>';
					echo '</td>';
					echo '<td>';
						echo self::getFieldInput( $field, $properties );
					echo '</td>';
				echo '</tr>';
		}
		echo '</table>';
		echo '</form>';
		echo '<button class="btn save low">Speichern</button>';
		echo '<button class="btn cancel low">Abbrechen</button>';
		echo '</div>';
	}
	
	private static function getFieldInput( $field, $properties, $value=null ) {
		
		$matches = array();
		preg_match('/(.*?)\(/', $properties["column_type"], $matches);
		
		$disabled = "";
		if($field == static::$primaryKey) {
			$disabled = "disabled";
		}
		if(array_key_exists(1, $matches)) {
			$type = $matches[1];
		}else{
			$type = $properties["column_type"];
		}
		$classes = "low inputfield";
		switch( $type ) {
			case "int":
					
				$return = '<input class="' . $classes . '" value="" type="number" id="' . $field . '" name="' . $field . '" '.$disabled.'>';
				break;
				
			case "varchar":
				$length = array();
				preg_match('/([0-9]+)/', $properties["column_type"], $length);
				$length = $length[1];
				$return = '<input class="' . $classes . '" type="text" value="" id="'.$field.'" maxlength="'.$length.'" name="'.$field.'"  '.$disabled.'>';
				break;
			case "enum":
				$enums = array();
				preg_match_all( '/\'(.*?)\'/', $properties["column_type"], $enums );
				$return = '<select class="' . $classes . '" id="'.$field.'" name="'.$field.'" '.$disabled.' >';
				
				if(count($enums) > 0) {
					foreach( $enums[1] as $enum ) {
						$return .= '<option value="'.$enum.'">'.$enum.'</option>';
					}
				}
				$return .= '</select>';
				break;
			case "text":
				$return = '<textarea class="'.$classes.'" id="'.$field.'" name="'.$field.'" '.$disabled.' ></textarea>';
				
				break;
			default:
				$return = '<input class="' . $classes . '" value="" type="text" id="' . $field . '" name="' . $field . '" '.$disabled.'>';
				break;
		}
		return $return;
	}
}