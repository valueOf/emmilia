<?php
class Mitarbeiter extends DBController {
	
	public static $table = "mitarbeiter";
    protected static $database = "emmilia";
	/**
	 * Primarykeys
	 */
    public static $primaryKey = "Personalnummer";
	protected static $fields;
	
	protected static $migrationSQL = "CREATE TABLE mitarbeiter (
	  Personalnummer NUMERIC(38) NOT NULL AUTO_INCREMENT PRIMARY KEY,
	  Nachname VARCHAR(50) NOT NULL,
	  Vorname VARCHAR(50) NOT NULL,
	  Adresse VARCHAR(50) NOT NULL,
	  PLZ NUMERIC(38) NOT NULL,
	  Ort VARCHAR(50) NOT NULL,
	  Email VARCHAR(50) NOT NULL,
	  Hash VARCHAR(128) NOT NULL,
	  Salt VARCHAR(128) NOT NULL,
	  Username VARCHAR(50) NOT NULL,
	  Power ENUM('Lieferbote', 'Verkäufer', 'Admin') NOT NULL
	);";
	
	/**
	 * Tablefields
	 */
	public $Personalnummer;
	public $Vorname;
	public $Nachname;
	public $Adresse;
	public $PLZ;
	public $Ort;
	public $Email;
	public $Hash;
	public $Salt;
	public $Username;
	public $Passwort;
	public $Power;
	
	public function save() {
		// Wenn ein neues Passwort gesetzt, dann neuen Salt erstellen und hashen!
		if( $this->Passwort ) {
			$this->Salt = self::createSalt();
			$this->Hash = self::HashPasswort($this->Passwort, $this->Salt);
		}
		
		parent::save();
	}
	
	private static function HashPasswort( $passwort, $salt ) {
		return $hash = hash_pbkdf2( HASH_ALGO , $passwort , PEPPER . $salt, HASH_ITERATIONS, 64);
	}
	
	private static function createSalt() {
	    $characters = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
	    
	    $charactersLength = strlen($characters);
	    $randomString = "";
	    for ($i = 0; $i < SALT_LENGTH; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    return $randomString;
	}
	
	public static function authenticate( $Username, $Passwort) {
		$DB_Connection = self::getPdoConnection();
		$stmt = $DB_Connection->prepare( "SELECT Salt, Hash, ". static::$primaryKey ." FROM " . static::$database. "." . static::$table . " WHERE Username LIKE :Username ");
		$stmt->bindParam(":Username", $Username);
		$stmt->execute();
		
		$Response = $stmt->fetch();
		
		// Wenn kein $Response dann Nutzer mit Username nicht vorhanden
		if(!$Response) return false;		
		
		$hash = self::HashPasswort( utf8_encode($Passwort), utf8_encode($Response["Salt"]));
		
		if( $hash != $Response["Hash"] ) return false;
		return $Response[static::$primaryKey];
		
	}
	
	public static function getFormularFields() {
		$fields = static::getFields();
		$array = array("Passwort" => array( "column_type" => "varchar(32)"));
		return array_merge($fields, $array);
	}
	/**
	 * 
	 * @param unknown $allowed
	 */
	public static function setRestrictionTo( $allowed ) {
		if( isset($_SESSION["USER_ID"])) {
			$Mit = Mitarbeiter::get($_SESSION["USER_ID"]);
			if( !in_array($Mit->Power, $allowed ) ) {
				header("Location: " . APP_BASE_URL );
			}
		}
	}
	
}