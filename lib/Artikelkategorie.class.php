<?php
class Artikelkategorie extends DBController {
	
	public static $table = "artikelkategorie";
    protected static $database = "emmilia";
	/**
	 * Primarykeys
	 */
    public static $primaryKey = "Kategorie_ID";
	protected static $fields;
	
	protected static $migrationSQL = "CREATE TABLE artikelkategorie (
	  Kategorie_ID NUMERIC(38) NOT NULL AUTO_INCREMENT PRIMARY KEY,
	  Name VARCHAR(50) NOT NULL,
	  Farbkennung VARCHAR(50) NOT NULL -- #FFD700
	);";
	
	/**
	 * Tablefields
	 */
	public $Kategorie_ID;
	public $Name;
	public $Farbkennung;
	
	public function delete() {

		$Artikel = Artikel::getBy( "Kategorie_ID=:Kategorie", array(":Kategorie" => $this->Kategorie_ID) );
		foreach( $Artikel as $Art ) {
			$Art->Kategorie_ID = "%NULL%";
			$Art->save();
		}
		parent::delete();
	}
	
}