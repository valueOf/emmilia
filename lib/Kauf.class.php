<?php
class Kauf extends DBController {
	
	public static $table = "kauf";
    protected static $database = "emmilia";
    public static $primaryKey = "Kaufnummer";
	protected static $fields;
	
	protected static $migrationSQL = "CREATE TABLE kauf (
	  Kaufnummer NUMERIC(38) NOT NULL AUTO_INCREMENT PRIMARY KEY,
	  Lieferung ENUM('intern', 'extern') NOT NULL, -- intern = Laden, extern = Telefon
	  Total DECIMAL(10, 2) NOT NULL,
	  Kaufzeitpunkt DATETIME NOT NULL,
	  Status  ENUM('In Bearbeitung', 'Abgeschlossen') NOT NULL,
	  Personalnummer NUMERIC(38) NOT NULL,
	  Kundennummer NUMERIC(38) NOT NULL,
	  CONSTRAINT fk_kauf_mitarbeiter FOREIGN KEY (Personalnummer) REFERENCES mitarbeiter(Personalnummer),
	  CONSTRAINT fk_kauf_kunde FOREIGN KEY (Kundennummer) REFERENCES kunde(Kundennummer)
	);";
	
	/**
	 * Tablefields
	 */
	public $Kaufnummer;
	public $Lieferung;
	public $Total;
	public $Kaufzeitpunkt;
	public $Status;
	public $VerkauftVon;
	public $Versandkosten;
	public $Kundennummer;
	
}