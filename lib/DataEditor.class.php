<?php
class DataEditor {
	
	public $Klasse;
	public $title;
	public $Callbacks = array();
	public $hiddenFields = array();
	public $formPath;
	public $searchField = true;
	public $data = false;
	public $additionalFields = array();
	public $tableFields;
	public $allowRemove = true;
	public $allowEdit = true;
	public $allowNew = true;
	
	private $rows = array();
	
	public function __construct( $Klasse, $title, $dir ) {
		$this->Klasse 	= $Klasse;
		$this->formPath = $dir;
		$this->title 	= $title;
		$this->tableFields = array_keys( $Klasse::getFields() );
	}
	
	/**
	 * Hookfunction um einzelne td's display Wert zu manipulieren
	 * @param string $Property
	 * @param string $callback
	 */
	public function filterOutput( $Property, $callback ) {
		$this->Callbacks[$Property] = $callback;
	}
	
	/**
	 * Hookfunction um neue Row hinzuzufügen
	 * @param string $Property
	 * @param string $callback
	 */
	public function addRow( $Title, $callback ) {
		$this->rows[$Title] = $callback;
	}
	/**
	 * Nach Hook Event Callback suchen und ausgeben falls vorhanden
	 * @param string $Property
	 */
	private function checkFieldOutput( $Property ) {
		if( array_key_exists( $Property, $this->Callbacks ) ){
			return $this->Callbacks[$Property];
		}
		return false;
	}
	/**
	 * HTML Table
	 */
	public function displayTable() { 
			$Klasse = $this->Klasse;
			$tables = new $Klasse();
			
		?>
		
		<div class="maincontent">
		    <h2><?= $this->title?></h2>
			<?php if($this->searchField && !$this->data) : ?>
			<form action="" method="GET">
				<label>
					Suchen nach: 
					<input value="<?= isset($_GET["q"]) ? $_GET["q"] : "";?>" class="low" name="q" type="text" >
					<?php if(isset($_GET["q"])) :?>
					<?php endif;?>
					<button class="btn low" type="submit" >Nicht suchen, finden!</button>
				</label>
			</form>
			<hr>
			<?php endif;
			if($this->allowNew) :
			?>
			<button class="button btn high edit" >Neuen <?= $Klasse?> anlegen<div class="color-rippling-container"></div></button>
			<?php 
			endif;
				$search = false;
				if( isset($_GET["q"]) ) {
					$search = $_GET["q"];
				}
				
				
				if( false !== $this->data) {
					$AllObjects = $this->data;					
				}else{
					$AllObjects = $Klasse::search( $search );
				}
				
				echo '<input type="hidden" id="data_class" value="' . $Klasse. '">';
				echo '<table id="'.$Klasse.'" class="view low">';
				echo '<tr class="view-header">';
				/* Table Head */
				foreach( $this->tableFields as $field ) {
					if(in_array($field, $this->hiddenFields)) continue;
					echo '<th data-name="' . $field . '" >' . $field . '<div class="sort"></div><div class="color-rippling-container"></div></th>';
				}
				if( $this->additionalFields ) {
					foreach($this->additionalFields as $addField=>$content) {
						echo '<th>'.$addField.'<div class="color-rippling-container"></div></th>';
					}
				}
				foreach($this->rows as $field=>$val ) {
					echo '<th>'.$field.'<div class="color-rippling-container"></div></th>';
				}
				if($this->allowEdit) {
					echo '<th>Editieren<div class="color-rippling-container"></div></th>';
				}
				if($this->allowRemove) {
					echo '<th>Löschen<div class="color-rippling-container"></div></th>';
				}
					
				
				echo '</tr>';
				if($AllObjects) {

					/* Table Content */
					foreach( $AllObjects as $Object ) {
						echo '<tr class="view-single">';
						foreach( $this->tableFields as $field ) {
							if(in_array($field, $this->hiddenFields)) continue;
							$class = "";
							echo '<td class="' .  $class .'" data-value="'.htmlspecialchars($Object->$field).'" data-name="'.$field.'">';
							
							if( $callback = $this->checkFieldOutput( $field ) ) {
								echo $callback( $Object->$field );
							}else{
								echo $Object->$field;	
							}
							
							echo "</td>";
							
						}
						if( $this->additionalFields ) {
							foreach($this->additionalFields as $addField=>$content) {
								echo '<td>'.$content.'</td>';
							}
						}
						foreach($this->rows as $field=>$val ) {
							echo '<td>' . $val($Object) . '</td>';
						}
						if($this->allowEdit) {
							echo '<td><a class="edit" href="#"></a></td>';
						}
						if($this->allowRemove) {
							echo '<td><a class="delete" href="#"></a></td>';
						}
						echo "</tr>";
					}
				}
				echo '</table>';
				include_once( "{$this->formPath}/Form.include.php" );
			?>
	</div>
    <div class="color-rippling-container"></div>
	<?php 
	}
}