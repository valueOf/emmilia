<?php
class Kunde extends DBController {
	
	public static $table = "kunde";
    protected static $database = "emmilia";
    public static $primaryKey = "Kundennummer";
	protected static $fields;
	
	protected static $migrationSQL = "CREATE TABLE kunde (
	  Kundennummer NUMERIC(38) NOT NULL AUTO_INCREMENT PRIMARY KEY,
	  Nachname VARCHAR(50) NOT NULL,
	  Vorname VARCHAR(50) NOT NULL,
	  Adresse VARCHAR(50) NOT NULL,
	  PLZ NUMERIC(38) NOT NULL,
	  Ort VARCHAR(50) NOT NULL,
	  Telefon VARCHAR(50) NOT NULL -- +49 15772862144
	);";
	
	/**
	 * Tablefields
	 */
	public $Kundennummer;
	public $Vorname;
	public $Nachname;
	public $Adresse;
	public $PLZ;
	public $Ort;
	public $Email;
	public $Telefon;	
	
}