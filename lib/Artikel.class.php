<?php
class Artikel extends DBController {
	
	public static $table = "artikel";
    protected static $database = "emmilia";
	/**
	 * Primarykeys
	 */
    public static $primaryKey = "Artikelnummer";
	protected static $fields;
	
	protected static $migrationSQL = "CREATE TABLE artikel (
	  Produktnummer NUMERIC(38) NOT NULL AUTO_INCREMENT PRIMARY KEY,
	  Preis int(11) NOT NULL,
	  Name VARCHAR(50) NOT NULL,
	  Barcode VARCHAR(50) NOT NULL,
	  Lagerbestand int(11) NOT NULL DEFAULT(0),
	  Kategorienummer NUMERIC(38) NOT NULL,
	  CONSTRAINT fk_produkt_kategorie FOREIGN KEY (Kategorie_ID) REFERENCES kategorie(Kategorie_ID)
	);";
	
	/**
	 * Tablefields
	 */
	public $Artikelnummer;
	public $Preis;
	public $Name;
	public $Barcode;
	public $Lagerbestand;
	public $Kategorie_ID;
	public $Status;
	
	public function delete() {
		trigger_error($this->Artikelnummer);
		$this->Status = 'Inaktiv';
		$this->save();
	}
	public static function checkBestand($Artikelnummer, $Artikelanzahl) {
		$Artikel 		= Artikel::get($Artikelnummer);
		return !!( $Artikelanzahl <= $Artikel->Lagerbestand );
	}
	
}