<?php
class WarenkorbPosten extends DBController {
	
	public static $table = "warenkorbposten";
    protected static $database = "emmilia";
    public static $primaryKey = "Warenkorbpostennummer";
	protected static $fields;
	
	protected static $migrationSQL = "CREATE TABLE warenkorbposten (
      Warenkorbpostennummer NUMERIC(38) NOT NULL AUTO_INCREMENT PRIMARY KEY,
	  Produktnummer NUMERIC(38) NOT NULL,
	  Kaufnummer NUMERIC(38) NOT NULL,
	  Produktanzahl NUMERIC(38) NOT NULL,
	  Preis int(11) NOT NULL,
	  CONSTRAINT fk_warenkorbposten_kauf FOREIGN KEY (Kaufnummer) REFERENCES kauf(Kaufnummer),
	  CONSTRAINT fk_warenkorbposten_produkt FOREIGN KEY (Produktnummer) REFERENCES produkt(Produktnummer)
	);";
	
	/**
	 * Tablefields
	 */
	public $Warenkorbpostennummer;
	public $Artikelnummer;
	public $Kaufnummer;
	public $Produktanzahl;
	public $Preis;
	
	public function LagerbestandAbziehen() {
		
	}
	
}