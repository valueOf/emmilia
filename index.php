<?php
    $title = "Login";
    require_once($_SERVER["DOCUMENT_ROOT"] ."/inc/app.settings.php");
    
	require_once( $_SERVER["DOCUMENT_ROOT"] ."/inc/Header.include.php" );
	
	//SUPER GEHEIMES DEBUGGIN STUFF
	if( isset( $_GET["DEBUG"] ) ) {
		$neuerAdmini = new Mitarbeiter();
		$neuerAdmini->Power = "Admin";
		$neuerAdmini->Passwort = "Admin";
		$neuerAdmini->Username = "Admin";
		$neuerAdmini->save();
	}
	
	//Login
	if( isset( $_POST["userName"] ) ) {
		$auth = Mitarbeiter::authenticate( $_POST["userName"], $_POST["userPassword"] );
		if( false !== $auth ) {
			$_SESSION["USER_ID"] = $auth;
			header( "Location: /Verwaltung/" );
			exit;
		}
		$error_msg = "Username oder Passwort sind inkorrekt";
	}
	if( isset( $_POST["logout"] ) ) {
		session_destroy();
		session_start();
	}
	
?>
    <script type="text/javascript" src="resources/js/Helper.js"></script>
    <link type="text/css" rel="stylesheet" href="resources/css/Main.css">
    <link type="text/css" rel="stylesheet" href="resources/css/UserLogin.css">
    <link rel="shortcut icon" href="resources/img/kolibri4.png"/>

    <section id="userLogin" class="high">
        <!-- form action TBD -->
        <form name="login" action="" method="post">
        
        <?php if( isset( $_SESSION["USER_ID"] ) ):
        	$Mitarbeiter = Mitarbeiter::get( $_SESSION["USER_ID"] ); 
        ?>
        	<h2> Sie sind eingeloggt als <strong><?= $Mitarbeiter->Vorname?></strong></h2>
        	<input type="hidden" name="logout">
        	<button class="low color-rippling-container" type="submit">Abmelden</button>
        	
        <?php else :?>
        
            <input value="<?= isset($_POST["userName"]) ? $_POST["userName"] : "";?>" class="low" type="text" name="userName" placeholder="Benutzername" required>
            <input class="low" type="password" name="userPassword" placeholder="Passwort" required>
            <button class="low color-rippling-container" type="submit" >Login<div class="color-rippling-container"></div></button>
        
        <?php endif;?>
        
        </form>
        <div class="color-rippling-container"></div>
    </section>
    <?php if( isset( $error_msg ) ) :?>
    
		<section class="alertbox bottom high">
			<p><?= $error_msg ?></p>
	        <div class="color-rippling-container"></div>
		</section>    
		
    <?php endif;?>
    
    <div class="color-rippling-container"></div>

<?php
	require_once("/inc/Footer.include.php");
?>
