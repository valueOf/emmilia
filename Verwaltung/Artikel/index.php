<?php
$Klasse = "Artikel";
$title = "Artikel";
require_once($_SERVER["DOCUMENT_ROOT"] ."/inc/app.settings.php");
/**
 * Restrictions
 */
Mitarbeiter::setRestrictionTo( array( "Admin" ) );

/**
 * Settings für Editor
 */

require_once($_SERVER["DOCUMENT_ROOT"] ."/inc/Header.include.php");


function getKategorieByID( $value ) {
	if( $value ) {
		return Artikelkategorie::get($value)->Name;
	}
}
function getPreisEuro( $value ) {
	$euro = $value/100;
	return "$euro €";
}


$editor = new DataEditor( $Klasse, $title, __DIR__ );
$editor->hiddenFields = array("Hash", "Salt", "Status");
$editor->filterOutput( "Kategorie_ID", "getKategorieByID" );
$editor->filterOutput( "Preis", "getPreisEuro" );

$editor->data = Artikel::getBy("Status='Aktiv'");
$editor->displayTable();



require_once($_SERVER["DOCUMENT_ROOT"] ."/inc/Footer.include.php");

