

<form id="form_mitarbeiter" class="edit-form">

    <div class="close-form"></div>
    <div class="input-wrapper one-quarter">
        <label for="ku_first_name">Artikelnummer</label>
        <input type="number" class="inputfield low" name="Artikelnummer" disabled>
    </div>
    <div class="input-wrapper three-quarter">
        <label for="ku_first_name">Name</label>
        <input type="text" class="inputfield low" name="Name">
    </div>

    <div class="input-wrapper half">
        <label for="ku_adress_street">Barcode</label>
        <input type="text" class="inputfield low" name="Barcode">
    </div>
    <div class="input-wrapper half">
        <label for="ku_adress_postalcode">Produktkategorie</label>
        <select class="inputfield low" name="Kategorie_ID" required>
        <?php 
        	$Kategorien = Artikelkategorie::getAll();
        	$primaryKey = Artikelkategorie::$primaryKey;
        	foreach($Kategorien as $Kategorie) :
        ?>
        	<option value="<?= $Kategorie->$primaryKey?>"><?= $Kategorie->Name?></option>
        <?php endforeach;?>
        </select>
    </div>
    <div class="input-wrapper one-third">
        <label for="ku_last_name">Preis <strong>(in Cent)</strong></label>
        <input type="number" class="inputfield low" name="Preis">
    </div>
    <div class="input-wrapper one-third">
        <label for="">MwSt.</label>
        <select class="inputfield low" name="Mwst">
        	<option value="0">Frei</option>
        	<option value="7">7%</option>
        	<option value="19">19%</option>
        </select>
    </div>
    <div class="input-wrapper one-third">
        <label for="ku_adress_city">Lagerbestand</label>
        <input type="text" class="Ort inputfield" name="Lagerbestand">
    </div>
    
    <div class="input-wrapper full low">
        <input type="submit" name="form-submit">
    </div>
    <div class="color-rippling-container"></div>
    
</form>