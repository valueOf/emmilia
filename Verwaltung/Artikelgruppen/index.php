<?php
$Klasse = "Artikelkategorie";
$title = "Artikelkategorien";
require_once($_SERVER["DOCUMENT_ROOT"] ."/inc/app.settings.php");
/**
 * Restrictions
 */
Mitarbeiter::setRestrictionTo( array( "Admin" ) );

/**
 * Settings für Editor
 */

require_once($_SERVER["DOCUMENT_ROOT"] ."/inc/Header.include.php");

function showRGBKasten( $value ) {
	return '<div class="colorpreview" style="background: ' . $value . '"></div>';
}

$editor = new DataEditor( $Klasse, $title, __DIR__ );
$editor->searchField = false;
$editor->filterOutput( "Farbkennung", "showRGBKasten" );
$editor->displayTable();

require_once($_SERVER["DOCUMENT_ROOT"] ."/inc/Footer.include.php");

?>
<link type="text/css" rel="stylesheet" href="./ressources/css/spectrum.css">
<script type="text/javascript" src="./ressources/js/spectrum.js"></script>
<script>
$(document).ready(function() {

	$(".color").spectrum({
		preferredFormat: "hex",
	});
})
</script>
	
