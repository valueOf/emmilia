

<form id="form_mitarbeiter" class="edit-form">

    <div class="close-form"></div>
    <div class="input-wrapper one-quarter">
        <label for="ku_first_name">Kategorie_ID</label>
        <input type="number" class="inputfield low" name="Kategorie_ID" disabled>
    </div>
    <div class="input-wrapper three-quarter">
        <label for="ku_first_name">Name</label>
        <input type="text" class="inputfield low" name="Name">
    </div>

    <div class="input-wrapper half">
        <label for="ku_adress_street">Farbkennung</label>
        <input type="text" class="color inputfield low" name="Farbkennung">
        
    </div>
    <div class="input-wrapper full low">
        <input type="submit" name="form-submit">
    </div>
    <div class="color-rippling-container"></div>
</form>

