<?php
	require_once($_SERVER["DOCUMENT_ROOT"] ."/inc/app.settings.php");
    $title = "Startseite";
	require_once($_SERVER["DOCUMENT_ROOT"] ."/inc/Header.include.php");
    
?>

    <script type="text/javascript" src="../resources/js/Helper.js"></script>
    <link type="text/css" rel="stylesheet" href="../resources/css/Main.css">
    <link rel="shortcut icon" href="../resources/img/kolibri4.png"/>
    
    <section class='hubs low'>
        
        <article class='hub'>
            <div class="hub-wrapper low">
                <header>
                    <h4>Kasse</h4>
                </header>
                <ul>
                    <li><a class="low" href="./Kasse/">Abkassieren</a></li>
                    <li><a class="low" href="./Bestellungen/">Bestellhistorie</a></li>
                </ul>
                <div class="color-rippling-container"></div>
            </div>
        </article>

        <article class='hub'>
	            <div class="hub-wrapper low">
					<h2 class="vertical-center">Artikel</h2>
	               <ul >
	                    <li class="low"><a  href="Artikel/">Artikel</a></li>
	                    <li class="low"><a  href="./Artikelgruppen/">Artikelgruppen</a></li>
	                </ul>
	                <div class="color-rippling-container"></div>
	            </div>
        </article>

        <article class='hub'>
        	<a href="Kunden/">
	            <div class="hub-wrapper low">
					<h2 class="vertical-center">Kunden</h2>
	                <div class="color-rippling-container"></div>
	            </div>
        	</a>
        </article>

        <article class='hub'>
        	<a href="Mitarbeiter/">
	            <div class="hub-wrapper low">
	                <h2 class="vertical-center">Mitarbeiter</h2>
	        		<div class="color-rippling-container"></div>
	            </div>
        	</a>
        </article>
		<div class="color-rippling-container"></div>
    </section>
    <div class="color-rippling-container"></div>

    <script type="text/javascript" src="../resources/js/Login.js"></script>

<?php
	require_once($_SERVER["DOCUMENT_ROOT"] . "/inc/Footer.include.php");
?>