

<form id="form_mitarbeiter" class="edit-form">

    <div class="close-form"></div>
        <div class="input-wrapper one-third">
        <label for="ku_first_name">Personalnummer</label>
        <input type="number" class="inputfield low" name="Personalnummer" disabled>
    </div>
    <div class="input-wrapper half">
        <label for="ku_first_name">Vorname</label>
        <input type="text" class="Vorname inputfield" name="Vorname">
    </div>
    <div class="input-wrapper half">
        <label for="ku_last_name">Nachname</label>
        <input type="text" class="Nachname inputfield" name="Nachname">
    </div>
    <div class="input-wrapper three-quarter">
        <label for="ku_adress_street">Adresse</label>
        <input type="text" class="Adresse inputfield" name="Adresse">
    </div>
    <div class="input-wrapper two-thirds">
        <label for="ku_adress_city">Ort</label>
        <input type="text" class="Ort inputfield" name="Ort">
    </div>
    <div class="input-wrapper one-third">
        <label for="ku_adress_postalcode">PLZ</label>
        <input type="number" class="PLZ inputfield" name="PLZ">
    </div>
    <div class="input-wrapper full">
        <label for="">Email</label>
        <input type="text" class="Email inputfield" name="Email">
    </div>
    <hr>
    <div class="input-wrapper one-third">
        <label for="">Username</label>
        <input type="text" class="inputfield low" name="Username">
    </div>
    <div class="input-wrapper one-third">
        <label for="">Neues Passwort</label>
        <input type="password" class="inputfield low" name="Passwort">
    </div>
    <div class="input-wrapper one-third">
        <label for="">Rechte</label>
        <select class="inputfield low" name="Power">
        	<option value="Seller">Verkäufer</option>
        	<option value="Admin">Admin</option>
        </select>
    </div>
    <div class="input-wrapper full low">
        <input type="submit" name="form-submit">
    </div>
    <div class="color-rippling-container"></div>
</form>