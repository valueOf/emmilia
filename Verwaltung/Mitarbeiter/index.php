<?php
	$Klasse = "Mitarbeiter";
    $title = "Mitarbeiter";
    require_once($_SERVER["DOCUMENT_ROOT"] ."/inc/app.settings.php");
    /**
     * Restrictions
     */
    Mitarbeiter::setRestrictionTo( array( "Admin" ) );
    
    /**
     * Settings für Editor
     */

    require_once($_SERVER["DOCUMENT_ROOT"] ."/inc/Header.include.php");
    
    $editor = new DataEditor( $Klasse, $title, __DIR__ );
    $editor->hiddenFields = array("Hash", "Salt");
    $editor->displayTable();
	
    require_once($_SERVER["DOCUMENT_ROOT"] ."/inc/Footer.include.php");
    
	