<?php
	$Klasse = "Kunde";
    $title = "Kunden";
    require_once($_SERVER["DOCUMENT_ROOT"] ."/inc/app.settings.php");
    /**
     * Restrictions
     */
    Mitarbeiter::setRestrictionTo( array( "Admin" ) );
    
    /**
     * Settings für Editor
     */

    require_once($_SERVER["DOCUMENT_ROOT"] ."/inc/Header.include.php");
    
    $editor = new DataEditor( $Klasse, $title, __DIR__ );
    
    $editor->addRow( "Bestellhistorie", "getBestellHistorieLink");
    
    function getBestellHistorieLink($Object) {
    	$Kdnr = $Object->Kundennummer;
    	return '<a class="link_orders" href="./Bestellhistorie.php?Kundennummer=' . $Kdnr . '"></a>';
    }
    $editor->displayTable();
	
    require_once($_SERVER["DOCUMENT_ROOT"] ."/inc/Footer.include.php");
    
    ?>
    
	