<?php
	$Klasse = "Kauf";
    $title = "Kundenbestellhistorie";
    require_once($_SERVER["DOCUMENT_ROOT"] ."/inc/app.settings.php");
    /**
     * Restrictions
     */
    Mitarbeiter::setRestrictionTo( array( "Admin", "Seller" ) );

    function getSellerUsername( $value ) {
    	return Mitarbeiter::get($value)->Username;
    }
    function getPreisEuro( $value ) {
    	$euro = $value/100;
    	return "$euro €";
    }
    
    require_once($_SERVER["DOCUMENT_ROOT"] ."/inc/Header.include.php");
    /**
     * Settings für Editor
     */
    $editor = new DataEditor( $Klasse, $title, __DIR__ );
    $editor->allowEdit = true;
    $editor->allowRemove = false;
    $editor->searchField = false;
    $editor->allowNew = false;
    
    $editor->hiddenFields = array("Kundennummer", "Kaufnummer");
    $editor->filterOutput("VerkauftVon", "getSellerUsername");
    $editor->data = Kauf::getBy("Kundennummer=:Kundennummer", array(":Kundennummer" => $_GET["Kundennummer"]));
    $editor->displayTable();
	
    require_once($_SERVER["DOCUMENT_ROOT"] ."/inc/Footer.include.php");
    
    
    
	