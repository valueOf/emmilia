<form id="form_kunden" class="edit-form">
    <div class="close-form"></div>
        <div class="input-wrapper one-third">
        <label for="ku_first_name">Kundennummer</label>
        <input type="number" class="Kundennummer inputfield" name="Kundennummer" disabled>
    </div>
    <div class="input-wrapper half">
        <label for="ku_first_name">Vorname</label>
        <input type="text" class="Vorname inputfield" name="Vorname">
    </div>
    <div class="input-wrapper half">
        <label for="ku_last_name">Nachname</label>
        <input type="text" class="Nachname inputfield" name="Nachname">
    </div>
    <div class="input-wrapper half">
        <label for="ku_adress_street">Adresse</label>
        <input type="text" class="Adresse inputfield" name="Adresse">
    </div>
    <div class="input-wrapper two-thirds">
        <label for="ku_adress_city">PLZ</label>
        <input type="text" class="PLZ inputfield" name="PLZ">
    </div>
    <div class="input-wrapper one-third">
        <label for="ku_adress_postalcode">Ort</label>
        <input type="text" class="Ort inputfield" name="Ort">
    </div>
    <div class="input-wrapper half">
        <label for="ku_telephone_number">Telefon</label>
        <input type="number" class="Telefon inputfield" name="Telefon">
    </div>
    <div class="input-wrapper half">
        <label for="ku_telephone_number">Email</label>
        <input type="text" class="Email inputfield" name="Email">
    </div>
    <div class="input-wrapper full low">
        <input type="submit" name="form-submit">
    </div>
    <div class="color-rippling-container"></div>
</form>