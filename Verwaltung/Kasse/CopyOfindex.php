<?php

    $title = "Bestellungen";

    $r = $_SERVER["DOCUMENT_ROOT"] . "/MittelstufenProjekt";

    $header = $r . "/Subpages/php/Header.include.php";
    if (file_exists($header)) {
        include_once($header);
    }
    
?>

<!--
    <title>Emmilia - Bestellungen</title>
    <script type="text/javascript" src="../../resources/js/Helper.js"></script>
    <link type="text/css" rel="stylesheet" href="../../resources/css/Main.css">
    <link type="text/css" rel="stylesheet" href="../../resources/css/ViewTables.css">
    <link rel="shortcut icon" href="../../resources/img/kolibri4.png"/>
 -->

    <h2>Bestellungen</h2>

   <table id="bestellungen" class="view">

        <tr class="view-header">
            <th data-name="or_id_number">KU-Nummer<div class="sort"></div></th>
            <th data-name="or_name">Name<div class="sort"></div></th>
            <th data-name="or_adress_street">Strasse, Nr.<div class="sort"></div></th>
            <th data-name="or_adress_postalcode">PLZ, Stadt<div class="sort"></div></th>
            <th data-name="or_telephone_number">Telefonnummer<div class="sort"></div></th>
            <th>Bestellungen</th>
            <th>Editeren</th>
            <th>Löschen</th>
        </tr>

        <tr class="view-single">
            <td data-name="or_id_number">0000001</td>
            <td data-name="or_name">Max Mustermann</td>
            <td data-name="or_adress_street">Musterstrasse 16</td>
            <td data-name="or_adress_postalcode">12345 Musterstadt</td>
            <td data-name="or_telephone_number">01234 56789</td>
            <td class='link_orders'><a href="#"></a></td>
            <td class="edit"><a href="#"></a></td>
            <td class="delete"><a href="#"></a></td>
        </tr>

        <tr class="view-single">
            <td data-name="or_id_number">0000002</td>
            <td data-name="or_name">Erika Mustermann</td>
            <td data-name="or_adress_street">Musterstrasse 16</td>
            <td data-name="or_adress_postalcode">12345 Musterstadt</td>
            <td data-name="or_telephone_number">01234 56789</td>
            <td class='link_orders'><a href="#"></a></td>
            <td class="edit"><a href="#"></a></td>
            <td class="delete"><a href="#"></a></td>
        </tr>
       
   </table>

<!-- 
    <script type="text/javascript" src="../../resources/js/ViewTableSort.js"></script>
-->

<?php
    $footer = $r . "/Subpages/php/Footer.include.php";
    if (file_exists($footer)) {
        include_once($footer);
    } 
?>