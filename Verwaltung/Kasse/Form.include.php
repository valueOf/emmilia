<?php
	require_once($_SERVER["DOCUMENT_ROOT"] ."/inc/app.settings.php");
    $title = "Startseite";
	require_once($_SERVER["DOCUMENT_ROOT"] ."/inc/Header.include.php");
	
	
	/* MYSQL Query result */

	/* 
	cat_colors:
		green: #39d639 
		blue: #4646ff
		red: #ff4949
		yellow: #c5c522
	*/
	$temp_products = array(
		"000001" => array(
			'name' => "Apfel",
			'price' => "0.39",
			'cat_color' => "lightseagreen",
			'cat' => "Obst",
			'available' => true,
			),
		"000002" => array(
			'name' => "Chips",
			'price' => "1.29",
			'cat_color' => "chocolate",
			'cat' => "Kartoffelprodukt",
			'available' => true,
			),
		"000003" => array(
			'name' => "Milch",
			'price' => "0.69",
			'cat_color' => "violet",
			'cat' => "Milchprodukt",
			'available' => true,
			),
		"000004" => array(
			'name' => "KÃ¤se",
			'price' => "2.59",
			'cat_color' => "violet",
			'cat' => "Milchprodukt",
			'available' => false,
			),
		"000005" => array(
			'name' => "Zeitung",
			'price' => "1.99",
			'cat_color' => "lightslategray",
			'cat' => "Lese/Schreibware",
			'available' => true,
			),
	);

?>

<form id="form_produkte" class="edit-form">

<div class="close-form"></div>

<div id="produkt_uebersicht">
<?php foreach ($temp_products as $id => $details) { ?>
	<?php 
		$class = $details['available'] ? 'available': 'unavailable';
		$name = $details['name'];
		$price = $details['price'];
	?>
	<div 
		class="produkt_item <?php echo $class; ?>" 
		data-name="<?php echo $name; ?>"
		data-price="<?php echo $price; ?>"
		style="background-color: <?php echo $details['cat_color'] ?>;">
		<div class="produkt_item_wrap">
			<div class="produkt_detail">
				<?php echo $name; ?>			
			</div>
		</div>
	</div>
<?php } ?>
</div>

<div class="input_list">
	
</div>

</form>

<script type="text/javascript">
	!function makeSquares() {
		var $produkt_items = $(".produkt_item");
		var $produkt_item_wraps = $(".produkt_item_wrap");

		var width = $produkt_items.first().outerWidth();
		$produkt_items.height(width+"px");

		$produkt_item_wraps.each(function(){
			$(this).css("top","calc(50% - " + $produkt_item_wraps.height()/2 + "px)");
		});
	}();

	$(function displayList() {
		$(".produkt_item.available").on("click", function() {
			$(".input_list").show();

			$(this).addClass("selected");
			$(this).unbind("click");

			var name = $(this).data("name");
			var price = $(this).data("price");

			function buildItem() {
				var item;
				item = "<div class=\"input-wrapper\" data-price=\""+price+"\">";
					item += "<span class=\"item_name\">"+name+" "+price+" â‚¬</span>";
			        item += "<label for=\"quantity_price\">";
			        	item += "<div class='add_item_qnt'></div>";
			        	item += "<div class='item_qnt'>1</div>";
			        	item += "<div class='subtract_item_qnt'></div>";
			        item += "</label>";
			        item += "<input disabled type=\"number\" name=\"quantity_price\" value=\""+price+"\">";
			    item += "</div>";
			    return item;
			}

			$(".input_list").append(buildItem());

			$(".add_item_qnt").on("click", function() {

				var pr = $(this).closest(".input-wrapper").data("price");
				var qn = parseInt($(this).next(".item_qnt").text());
				qn++;
				$(this).next(".item_qnt").text(qn);

				var tempPr = (pr*100) * qn;

				var fullPr = tempPr/100;

				$(this).parent().next("input").val(fullPr);
			});

			$(".subtract_item_qnt").on("click", function() {

				var pr = $(this).closest(".input-wrapper").data("price");
				var qn = parseInt($(this).prev(".item_qnt").text());
				qn--;
				if (qn > 0) {
					$(this).prev(".item_qnt").text(qn);

					var tempPr = (pr*100) * qn;

					var fullPr = tempPr/100;

					$(this).parent().next("input").val(fullPr);
				}
			});

		});
	});
</script>
<?php
	require_once($_SERVER["DOCUMENT_ROOT"] . "/inc/Footer.include.php");
?>