<?php
    require_once($_SERVER["DOCUMENT_ROOT"] . "/inc/app.settings.php");
    $title = "Kasse";
    
    require_once( $_SERVER["DOCUMENT_ROOT"] ."/inc/Header.include.php" );
?>
<script>
	var Seller 				= '<?= $_SESSION["USER_ID"];?>';
	var Versandkosten 		= '<?= VERSANDKOSTEN;?>';
	var Versandkostengrenze = '<?= VERSANDKOSTENGRENZE;?>';
</script>
<div id="kasse-wrapper">
	<section class="kasse produkte" id="products">
		<h2>Produkte</h2>
		<?php 
			$ProduktPrimaryKey = Artikel::$primaryKey;
			$Produkte = Artikel::join(
			/* Felder */	"Artikel.*, Artikelkategorie.Farbkennung",
			/* Tabelle */	"LEFT JOIN ",
			/* Join */		Artikelkategorie::$table,
			/* Using/On */	" USING(Kategorie_ID)",
					/* */   "WHERE Artikel.Status='Aktiv'",
			/* Order */		"ORDER BY Kategorie_ID"	);
			
			
			foreach( $Produkte as $Produkt ) : ?>
					<div 
					class="produkt_item low <?= !$Produkt->Lagerbestand || $Produkt->Lagerbestand <= 0? "disabled" : ""?>" 
					data-id="<?= $Produkt->$ProduktPrimaryKey?>"
					data-name="<?= $Produkt->Name?>"
					data-preis="<?= $Produkt->Preis?>"
					style="background-color: <?= $Produkt->Farbkennung?>;">
					<div class="produkt_item_wrap">
						<div class="produkt_detail">
							<?php 
								echo "<h3>{$Produkt->Name}</h3>"; 
								echo "<h3>"  .$Produkt->Preis/100 . "€</h3>";
							?>			
						</div>
					</div>
					<div class="color-rippling-container"></div>
				</div>
			<?php endforeach;	?>
			<div class="color-rippling-container"></div>
			<?php include_once(dirname(__FILE__) . "/Lieferung.php");?>
			
	</section>

	<section class="kasse warenkorb" id="warenkorb">
		<h2>Warenkorb</h2>
		<table id="warenkorbtable" class="low">
			<thead>
				<tr><th colspan="3" >Artikel</th><th>Preis</th></tr>
			</thead>
			<tbody></tbody>
			<tfoot>
				<tr class="versand-tr"><td colspan="3">Zzgl Versandkosten:</td><td id="versandkosten">0€</td>
				<tr><td colspan="3">Summe:</td><td id="warenkorb_summe">0€</td>
			</tfoot>
		</table>
		<button class="btn low rippling buydirekt">Ladenverkauf<div class="color-rippling-container"></div></button>
		<button class="btn low rippling buylieferung">Lieferung<div class="color-rippling-container"></div></button>
		<div class="color-rippling-container"></div>
		
	</section>
	<div class="color-rippling-container"></div>

</div>

<?php
    require_once($_SERVER["DOCUMENT_ROOT"] . "/inc/Footer.include.php");
?>
    <script type="text/javascript" src="<?= APP_BASE_URL?>resources/js/Kasse.js"></script>
