
<div class="edit-form" id="lieferungdialog">
	<h2>Lieferung</h2>
	<div class="input-wrapper half">
		<label for="newclient">
			Neuer Kunde 
		</label>
		<input id="newclient" class="clientradio" type="radio" name="client" value="new">
	</div>
	<div class="input-wrapper half">
		<label for="existingclient">
			Bestehender Kunde
		</label>
		<input checked="checked" id="existingclient" class="clientradio" type="radio" name="client" value="existing">
	</div>
	<section class="existing clientoption">
				<label>
					Suchen nach: 
					<input value="<?= isset($_GET["q"]) ? $_GET["q"] : "";?>" class="low" name="q" type="text" >
					<?php if(isset($_GET["q"])) :?>
					<?php endif;?>
				</label>
	</section>
	<section class="new clientoption">
		
	</section>
	<section class="clientsection">
			<form id="form_kunden" >
		        <div class="input-wrapper one-third">
		        <label for="ku_first_name">Kundennummer</label>
		        <input type="number" class="Kundennummer inputfield" name="Kundennummer" disabled>
		    </div>
		    <div class="input-wrapper half">
		        <label for="ku_first_name">Vorname</label>
		        <input type="text" class="Vorname inputfield" name="Vorname">
		    </div>
		    <div class="input-wrapper half">
		        <label for="ku_last_name">Nachname</label>
		        <input type="text" class="Nachname inputfield" name="Nachname">
		    </div>
		    <div class="input-wrapper half">
		        <label for="ku_adress_street">Adresse</label>
		        <input type="text" class="Adresse inputfield" name="Adresse">
		    </div>
		    <div class="input-wrapper two-thirds">
		        <label for="ku_adress_city">PLZ</label>
		        <input type="text" class="PLZ inputfield" name="PLZ">
		    </div>
		    <div class="input-wrapper one-third">
		        <label for="ku_adress_postalcode">Ort</label>
		        <input type="text" class="Ort inputfield" name="Ort">
		    </div>
		    <div class="input-wrapper half">
		        <label for="ku_telephone_number">Telefon</label>
		        <input type="number" class="Telefon inputfield" name="Telefon">
		    </div>
		    <div class="input-wrapper half">
		        <label for="ku_telephone_number">Email</label>
		        <input type="text" class="Email inputfield" name="Email">
		    </div>
		    <div class="input-wrapper full low">
		        <button type="button" class="buy btn button low">Kaufen</button>
		    </div>
		</form>
	    <div class="color-rippling-container"></div>
	</section>
</div>