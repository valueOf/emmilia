<?php
	$Klasse = "Kunde";
    $title = "Kunde der Bestellung #" . $_GET["Kaufnummer"];
    require_once($_SERVER["DOCUMENT_ROOT"] ."/inc/app.settings.php");
    /**
     * Restrictions
     */
    Mitarbeiter::setRestrictionTo( array( "Admin", "Seller" ) );

    function getSellerUsername( $value ) {
    	return Mitarbeiter::get($value)->Username;
    }
    function getPreisEuro( $value ) {
    	$euro = $value/100;
    	return "$euro €";
    }
    
    require_once($_SERVER["DOCUMENT_ROOT"] ."/inc/Header.include.php");
    /**
     * Settings für Editor
     */
    $editor = new DataEditor( $Klasse, $title, __DIR__ );
    $editor->allowEdit = false;
    $editor->allowRemove = false;
    $editor->searchField = false;
    $editor->allowNew = false;
    
    $editor->displayTable();
	
    require_once($_SERVER["DOCUMENT_ROOT"] ."/inc/Footer.include.php");
    
    
    
	