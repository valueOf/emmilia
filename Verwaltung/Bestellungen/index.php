<?php
	$Klasse = "Kauf";
    $title = "Bestellhistorie";
    require_once($_SERVER["DOCUMENT_ROOT"] ."/inc/app.settings.php");
    /**
     * Restrictions
     */
    Mitarbeiter::setRestrictionTo( array( "Admin", "Seller" ) );

    function getDetailLink( $Object ) {
    	
    }
    function getSellerUsername( $value ) {
    	return Mitarbeiter::get($value)->Username;
    }
    require_once($_SERVER["DOCUMENT_ROOT"] ."/inc/Header.include.php");
    /**
     * Settings für Editor
     */
    $editor = new DataEditor( $Klasse, $title, __DIR__ );
    $editor->allowRemove = false;
    $editor->searchField = false;
    $editor->allowNew = false;
    
    
    $editor->addRow("Details", "getDetailLink");
    
    
    $editor->hiddenFields = array("Kundennummer");
    $editor->filterOutput("VerkauftVon", "getSellerUsername");
    $editor->displayTable();
	
    require_once($_SERVER["DOCUMENT_ROOT"] ."/inc/Footer.include.php");
    
    
    
	