

<form id="form_mitarbeiter" class="edit-form">

    <div class="close-form"></div>
    <div class="input-wrapper one-quarter">
        <label for="ku_first_name">Kaufnummer</label>
        <input type="number" class="inputfield low" name="Kaufnummer" disabled>
    </div>

    <div class="input-wrapper half">
        <label for="ku_adress_street">Status</label>
        <select class="color inputfield low" name="Status">
        	<option value="In Bearbeitung">In Bearbeitung</option>
        	<option value="In Bearbeitung">Abgeschlossen</option>
        </select>
        
    </div>
    <div class="input-wrapper full low">
        <input type="submit" name="form-submit">
    </div>
    <div class="color-rippling-container"></div>
</form>

