﻿DROP DATABASE IF EXISTS emmilia;
CREATE DATABASE IF NOT EXISTS emmilia;
use emmilia;

CREATE TABLE kunde (
	  Kundennummer INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
	  Nachname VARCHAR(50) NOT NULL,
	  Vorname VARCHAR(50) NOT NULL,
	  Adresse VARCHAR(50) NOT NULL,
	  PLZ NUMERIC(38) NOT NULL,
	  Ort VARCHAR(50) NOT NULL,
	  Telefon VARCHAR(50) NOT NULL,
	  Email VARCHAR(50) NOT NULL
);

ALTER TABLE kunde ADD FULLTEXT `Fulltext` (`Nachname`, `Vorname`, `Adresse`, `Ort`);
##ALTER TABLE kunde ADD INDEX `PKIndex` (`Kundennummer`) USING BTREE;

CREATE TABLE mitarbeiter (
	  Personalnummer INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
	  Nachname VARCHAR(50) NOT NULL,
	  Vorname VARCHAR(50) NOT NULL,
	  Adresse VARCHAR(50) NOT NULL,
	  PLZ NUMERIC(38) NOT NULL,
	  Ort VARCHAR(50) NOT NULL,
	  Email VARCHAR(50) NOT NULL,
	  Hash VARCHAR(128) NOT NULL,
	  Salt VARCHAR(128) NOT NULL,
	  Username VARCHAR(50) NOT NULL,
	  Power ENUM('Lieferbote', 'Verkäufer', 'Admin') NOT NULL
);

ALTER TABLE mitarbeiter ADD FULLTEXT `Fulltext` (`Nachname`, `Vorname`, `Adresse`, `Ort`, `Username`);
##ALTER TABLE mitarbeiter ADD INDEX `PKIndex` Personalnummer USING BTREE;

CREATE TABLE artikelkategorie (
	  Kategorie_ID INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
	  Name VARCHAR(50) NOT NULL,
	  Farbkennung VARCHAR(50) NOT NULL -- #FFD700
);

CREATE TABLE artikel (
	  Artikelnummer INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
	  Preis int(11) NOT NULL,
	  Name VARCHAR(50) NOT NULL,
	  Barcode VARCHAR(50) NOT NULL,
	  Lagerbestand int(11) NOT NULL DEFAULT '0',
	  Kategorie_ID INT(11) Default NULL,
	  Status enum('Aktiv', 'Inaktiv') Default 'Aktiv',
	  CONSTRAINT fk_produkt_kategorie FOREIGN KEY (Kategorie_ID) REFERENCES artikelkategorie(Kategorie_ID)
);

ALTER TABLE artikel ADD FULLTEXT `Fulltext` (`Name`);
##ALTER TABLE artikel ADD INDEX `PKIndex` Artikelnummer USING BTREE;

CREATE TABLE kauf (
	  Kaufnummer INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
	  Lieferung ENUM('intern', 'extern') NOT NULL, -- intern = Laden, extern = Telefon
	  Total INT(11) NOT NULL,
	  Kaufzeitpunkt DATETIME NOT NULL,
	  Status  ENUM('In Bearbeitung', 'Abgeschlossen') NOT NULL,
	  VerkauftVon INT(11) DEFAULT NULL,
	  Kundennummer INT(11) DEFAULT NULL,
	  CONSTRAINT fk_kauf_mitarbeiter FOREIGN KEY (VerkauftVon) REFERENCES mitarbeiter(Personalnummer),
	  CONSTRAINT fk_kauf_kunde FOREIGN KEY (Kundennummer) REFERENCES kunde(Kundennummer)
);

CREATE TABLE warenkorbposten (
      Warenkorbpostennummer INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
	  Artikelnummer INT(11) NOT NULL,
	  Kaufnummer INT(11) NOT NULL,
	  Produktanzahl INT(11) NOT NULL,
	  Preis int(11) NOT NULL,
	  CONSTRAINT fk_warenkorbposten_kauf FOREIGN KEY (Kaufnummer) REFERENCES kauf(Kaufnummer),
	  CONSTRAINT fk_warenkorbposten_produkt FOREIGN KEY (Artikelnummer) REFERENCES artikel(Artikelnummer)
);

-- DUMMY DATEN

--
-- Daten für Tabelle `artikel`
--

INSERT
INTO
  `artikel`(
    `Artikelnummer`,
    `Preis`,
    `Name`,
    `Barcode`,
    `Lagerbestand`,
    `Status`,
    `Kategorie_ID`
  )
VALUES(
  3,
  1,
  'asd',
  '2342342f',
  1,
  'Inaktiv',
  NULL
),(
  5,
  1,
  'asd',
  'r345345f',
  46,
  'Inaktiv',
  NULL
),(
  6,
  1,
  'asd',
  '453543fg',
  56,
  'Inaktiv',
  NULL
),(
  7,
  99,
  'Snickers',
  '2342342f',
  33,
  'Aktiv',
  8
),(
  8,
  99,
  'Mars',
  '345353f',
  33,
  'Aktiv',
  8
),(
  9,
  99,
  'Energy Drink 0.5l',
  '3453geg',
  44,
  'Aktiv',
  9
),(
  10,
  129,
  'Apfelsaft 2l',
  'fsdf3',
  33,
  'Aktiv',
  9
),(
  11,
  129,
  'BILD Zeitung',
  'dfgdfg4',
  33,
  'Aktiv',
  7
),(
  12,
  129,
  'Bravo',
  '53453gt',
  32,
  'Aktiv',
  7
);


--
-- Daten für Tabelle `artikelkategorie`
--

INSERT
INTO
  `artikelkategorie`(
    `Kategorie_ID`,
    `Name`,
    `Farbkennung`
  )
VALUES(7, 'Zeitschriften', '#b1b1b1'),(8, 'Speisen', '#e1b220'),(9, 'Getränke', '#dd5050');


--
-- Daten für Tabelle `kunde`
--

INSERT
INTO
  `kunde`(
    `Kundennummer`,
    `Nachname`,
    `Vorname`,
    `Adresse`,
    `PLZ`,
    `Ort`,
    `Telefon`,
    `Email`
  )
VALUES(
  1,
  'Kauka',
  'Daniel',
  'Heinrich',
  '50354',
  '',
  '23423424',
  'dk@km-it.de'
),(
  25,
  'ghklökl',
  'Dfsd',
  'Heinrich Hörle Str., 2',
  '50354',
  '54564',
  '45645',
  'gdfghfghf'
),(
  26,
  '',
  '',
  'Am Lindengarten 4',
  '50374',
  'Erftstadt',
  '',
  'kanieldauka@live.de'
),(
  27,
  'ghjghjg',
  'gfhghj',
  'Am Lindengarten 4',
  '50374',
  'Erftstadt',
  '5464564564',
  'kanieldauka@live.de'
),(
  28,
  'fsfsdf',
  'sdfsdfsf',
  'sdfsfs',
  'fsfs',
  'sdfsdfs',
  '453535',
  'dfgdfgd'
),(
  29,
  'FFFF',
  'DDD',
  'Am Lindengarten 4',
  '50374',
  'Erftstadt',
  '235242',
  'kanieldauka@live.de'
),(
  30,
  'FFFF',
  'DDD',
  'Am Lindengarten 4',
  '50374',
  'Erftstadt',
  '235242',
  'kanieldauka@live.de'
),(
  31,
  'FFFF',
  'DDD',
  'Am Lindengarten 4',
  '50374',
  'Erftstadt',
  '235242',
  'kanieldauka@live.de'
);


--
-- Daten für Tabelle `mitarbeiter`
--

INSERT
INTO
  `mitarbeiter`(
    `Personalnummer`,
    `Nachname`,
    `Vorname`,
    `Adresse`,
    `PLZ`,
    `Ort`,
    `Email`,
    `Hash`,
    `Salt`,
    `Username`,
    `Power`
  )
VALUES(
  1,
  'Kauka',
  'Daniel',
  'Heinrich Hörle Str. 2',
  '50354',
  'Hürth',
  'kanieldauka@live.de',
  '7893db88cfd1e0f7bb6d112c7c1e0eef5eef8b952131765a23019b8823cb1693',
  'sa8xRxyATw1eht1zgdvbw7Yci0najdbd',
  'Admin',
  'Admin'
),(
  3,
  'Fitzgerald',
  'James',
  'Ich habe keine Ahnungweg',
  '1337',
  'Bonn',
  'Jimbo@jimbo.jimbo',
  '50a0bc8680082fd61d1009815f2cb92cb0fec73e809f32ac59c94787a0a966c1',
  'RhOcwv4FzNMjZLFugtW1lxjgH314iALK',
  'Jimbo',
  'Admin'
),(
  4,
  'Kratzsch',
  'Johannes',
  'Assistraße 1',
  '12345',
  'Köln ',
  'johannes.kratzsch@gmail.com',
  'a3c976cc8de956d99e0ebb9f3839618ffd0fc891a31da8b652f1ddb901ac156a',
  'SSO533nxocjvIieHnMdlcERazlgnhR1G',
  'Blondel',
  'Admin'
),(
  5,
  'Rogel wie Vogel nur mit R',
  'Karl',
  'Mortadellanudeln',
  '50674',
  'Köln warscheinlich',
  'karlrogelistkeinvogel@gmail.com',
  '86216176229930b99c6996f74808de296dddba0452489db5cff1606cb5cc3757',
  'CgMbDFMHNTY94w5RXElHoTsZRkEAkxDP',
  'Karl',
  'Admin'
),(
  6,
  'Karabulut',
  'Deniz',
  'Ich hoffe das ist richtig geschrieben',
  '12345',
  'Irgendwo in Deutschland',
  'asdf',
  'd4e0c721f655330017d37614bd2a2f0313b93e057bd7355390a3d0a6f4718694',
  'IFdkHHr1AsOYcUTTUvO2xe75rxWhsqSk',
  'Deniz',
  'Admin'
);

