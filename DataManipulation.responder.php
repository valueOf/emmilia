<?php
 require_once($_SERVER["DOCUMENT_ROOT"] . "/inc/app.settings.php" );
 
 if( !isset($_SESSION["USER_ID"]) ) {
 	header("Location: " . APP_BASE_URL );
 	exit();
 }
 header("Content-Type: application/json");
 
 
 if($_GET["action"]) {


 	switch($_GET["action"]) {
 		case "save":
 			$Klasse = $_POST["Klasse"];
 			$data = new $Klasse( $_POST );
 			$data->save();
			die(json_encode($data)); 	
 			break;
 		case "delete":
 			$Klasse = $_POST["Klasse"];
 			$data = new $Klasse( $_POST );
 			$data->delete();
 			die(json_encode(array("success" => true)));
 			break;
 			
 		case "lieferung":
 			$Artikel 	= $_POST["Artikel"];
 			$Seller 	= $_POST["Seller"];
 			
 			//LagerbestandCheck nötig!!
 			foreach($Artikel as $Key=>$Art) {
 				if( ! Artikel::checkBestand($Key, $Art["Produktanzahl"]) ) {
 					die(json_encode(array("success" => false, "msg"=>"Artikel {$Art["Name"]} Lagerbestand zu gering!")));
 				}
 			}
 			//Transaction wäre nett
 			
 			$Kunde 	 	= new Kunde( $_POST["Kunde"] ); 
 			if( ! $Kunde->save() ) {
 				die(json_encode(array("success" => false, "msg"=>"Kunde konnte nicht gespeichert werden")));
 			};
 			

 			
 			$Bestellung = new Kauf();
 			$Bestellung->Kaufzeitpunkt = date("Y-m-d h:i:s");
 			$Bestellung->VerkauftVon = (int)$Seller;
 			$Bestellung->Versandkosten = (int)$_POST["Versandkosten"];
 			$Bestellung->Total 			= (int)$_POST["Summe"];
 			$Bestellung->Kundennummer   = $Kunde->Kundennummer;
 			
 			if($_POST["Lieferung"] == "extern" ) {
	 			$Bestellung->Status    = "In Bearbeitung";
				$Bestellung->Lieferung = "extern"; 				
 			}else{
	 			$Bestellung->Status    = "Abgeschlossen";
				$Bestellung->Lieferung = "intern"; 				
 			}
 			if( ! $Bestellung->save() ) {
 				die(json_encode(array("success" => false, "msg"=>"Bestellung konnte nicht gespeichert werden")));
 			}
 			
 			/**
 			 * Artikel in die Warenkorbtabelle eintragen
 			 */
 			foreach( $Artikel as $Key=>$Posten ) {
 				$WarenkorbPosten = new WarenkorbPosten( $Posten );
 				$WarenkorbPosten->Kaufnummer = $Bestellung->Kaufnummer;
 				$WarenkorbPosten->Artikelnummer = $Key;
 				$WarenkorbPosten->Produktanzahl = $Posten["Produktanzahl"];
 				if( ! $WarenkorbPosten->save() ) {
 					die(json_encode(array("success" => false, "msg"=>"Warenkorb konnte nicht gespeichert werden")));
 				}
 			}
 			$WarenkorbPosten->LagerbestandAbziehen();
 			die(json_encode(array("success" => true)));
 			break;
 		case "checkBestand":
 				$Artikelnummer 	= $_POST['Artikelnummer'];
 				$Artikelanzahl 	= $_POST["Aritkelanzahl"];
 				
 				die(json_encode(Artikel::checkBestand($Artikelnummer, $Artikelanzahl)));;
 				
 			break;
 	}
 }

 
 